RobochickenBundle
=================

*She-Who-Lays-Many-Eggs*
*And Also NEEDS WAYYYYY More Business Logic*
------------------------

The RoboChicken is a special kind of bird and needs props for laying the most eggs. So, we devised and
coded this excellent web app to verify the validity of one's claims of possessing a true RoboChicken.

----

#Application Structure:#

    1. src/RobochickenBundle/Entity/Hen.php - Entity aka Object Model
    2. src/RobochickenBundle/Controller/PageController - Controller
    3. src/RobochickenBundle/Resources/views/Page/index.html.twig - View
    4. arc/RobochickenBundle/Resources/config/routing.yml - Routing
    5. app/Resources/views/base.html.twig - View
    6. src/app/config/routing.yml - Routing
    7. src/app/config/config.yml - Global Config
