<?php

namespace Jbalthis\RobochickenBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestControllerTest extends WebTestCase
{
    public function testRequest()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/request');
    }

    public function testResponse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/response');
    }

}
