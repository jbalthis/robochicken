var request;

$("#form").submit(function(event){

    if (request) {
        request.abort();
    }

    var $form = $(this);
    var $inputs = $form.find("input");
    var serializedData = $form.serialize();

    $inputs.prop("disabled", true);

    request = $.ajax({
        url: "/request",
        type: "get",
        data: serializedData
    });

    request.done(function (response, textStatus, jqXHR){
        alert("Hooray, it worked!"+response+":"+textStatus+":"+jqXHR);
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
                "The following error occured: "+
                textStatus, errorThrown
        );
    });

    request.always(function () {
        $inputs.prop("disabled", false);
    });

    event.preventDefault();
});