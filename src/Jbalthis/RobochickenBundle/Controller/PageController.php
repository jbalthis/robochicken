<?php

namespace Jbalthis\RobochickenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Jbalthis\RobochickenBundle\Entity\Hen;

class PageController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render('JbalthisRobochickenBundle:Page:index.html.twig');
    }

    /**
     * @Route("/create")
     */
    public function createAction(){

        // Just to show how to persist a new obj

        $hen = new Hen();
        $hen->setBreed('Rhode Island Red');

        $em = $this->getDoctrine()->getManager();
        $em->persist($hen);
        $em->flush();

        return new Response('Created a '.$hen->getBreed().' with ID '.$hen->getId());
    }



    /**
     * @Route("/show")
     */
    public function showAction()
    {
        // Just to show how to query via ORM
        $id = '1';
        $repo = $this->getDoctrine()
            ->getRepository('JbalthisRobochickenBundle:Hen');
        $breed = $repo->find($id);

        // and to show http req
        $req = new Request();
        $_breed = $req->get('breed');

        // granted the logic is simple but according to yahoo answers its true lol
        if($breed && $breed == 'Rhode Island Red' && $breed == $_breed){
            return new Response('True Dat!');
        }
        else{
            return new Response('Woff! :(');
        }
    }



}
